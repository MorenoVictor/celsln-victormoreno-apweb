<?php
session_start(); //se va a checar $_SESSION, se necesita 'session_start()'
//verificar que el cliente se ha logeado
if (isset($_SESSION['clienteLogeado']) && $_SESSION['clienteLogeado'] != null) {
    //si hay logeado, se muestra
    // header('location:./public/cart.php');
} else {
    //si no hay cliente logeado, se abre el login
    header('location:./login.php');
}

//se carga producto
//crear instancia de Productos
require_once dirname(dirname(__FILE__)) . '/app/middleware/productos/Producto.php';
// require_once dirname(dirname(dirname(__FILE__))) . '/app/middleware/bd/CriterioDeBusqueda.php';
$producto = new Producto('', '', '', '', '', '', '', '');


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>PDV UTH v1</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">
    <link href="css/price-range.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head>
<!--/head-->

<body>
    <!-- header --><?php include 'includes/header.php'; ?>


    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Carrito de Compras</li>
                </ol>
            </div>
            <div class="table-responsive cart_info">
                <table class="table table-condensed">
                    <thead>
                        <tr class="cart_menu">
                            <td class="image">Producto</td>
                            <td class="description"></td>
                            <td class="price">Precio</td>
                            <td class="quantity">Cantidad</td>
                            <td class="total">Total</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        $repetido = 0;
                        $subtotal = 0;
                        //NUEVO articulo recibido por GET
                        if (isset($_GET['producto_id'])) {
                            $prod_id = $_GET['producto_id'];
                            //se consultan los datos de Producto
                            $datosDeProducto = $producto->consultaPorID($prod_id)[0];
                            //se agrega a variable de sesion de carrito
                            if (isset($_SESSION['carrito'])) {
                                //verificar que no exista previamente en el carrito
                                foreach ($_SESSION['carrito'] as $index => $value) {
                                    if ($value['producto']['id'] == $prod_id) {
                                        $conta = $value['cantidad'] + 1;
                                        $value['cantidad'] = $conta;
                                        $repetido = 1;
                                        //se actualiza el valor cambiado en el foreach
                                        $_SESSION['carrito'][$index] = $value;
                                        // break;
                                    }
                                }
                                if ($repetido != 1) {
                                    //ya hay articulos, se saca el num de articulos en el arreglo
                                    $numArticulos = count($_SESSION['carrito']);
                                    //se agrega este nuevo Producto
                                    $prodCarrito = [
                                        "producto" => $datosDeProducto,
                                        "cantidad" => 1,
                                    ];
                                    //se agrega en el siguiente indice
                                    $_SESSION['carrito'][$numArticulos++] = $prodCarrito;
                                }
                            } else {
                                //Si es el primer articulo, se pone en el idx 0
                                //se agrega este nuevo Producto
                                $prodCarrito = [
                                    "producto" => $datosDeProducto,
                                    "cantidad" => 1,
                                ];
                                //se agrega en el siguiente indice
                                $_SESSION['carrito'][0] = $prodCarrito;
                            } //ya existe carrito?
                        } //if llega nuevo prod

                        if (isset($_SESSION['carrito'])) {
                            //se genera un renglon por cada articulo en la coleccion
                            foreach ($_SESSION['carrito'] as $key => $value) {
                                // var_dump($value['cantidad']);
                                # renglon por cada elemento
                        ?>

                        <tr>
                            <td class="cart_product">
                                <a href="???"><img width="20%"
                                        src="<?php echo $value['producto']['imagen_producto']; ?>" alt=""></a>
                            </td>
                            <td class="cart_description">
                                <h4><a href=""><?php echo $value['producto']['nombre']; ?></a></h4>
                                <p>Web ID: <?php echo $value['producto']['codigo_barras']; ?></p>
                                <p>ID: <?php echo $value['producto']['id']; ?></p>
                            </td>
                            <td class="cart_price">
                                <p>$<?php echo $value['producto']['precio']; ?></p>
                            </td>
                            <td class="cart_quantity">
                                <div class="cart_quantity_button">
                                    <a class="cart_quantity_up" href=""> + </a>
                                    <input class="cart_quantity_input" type="text" name="quantity"
                                        value=" <?php echo $value['cantidad']; ?>" autocomplete="off" size="2">
                                    <a class="cart_quantity_down" href=""> - </a>
                                </div>
                            </td>
                            <td class="cart_total">
                                <p class="cart_total_price">
                                    $<?php echo $subt = $value['cantidad'] * $value['producto']['precio']; ?></p>
                            </td>
                            <td class="cart_delete">
                                <a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
                            </td>
                        </tr>

                        <?php
                                //se actumula el subtotal
                                $subtotal += $subt;
                            } //fin de if agregar a carrito 
                        } //if isset carrito
                        else {
                            echo "<h1>CARRITO VACIO</h1>";
                        }
                        ?>

                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!--/#cart_items-->

    <section id="do_action">
        <div class="container">
            <div class="heading">
                <h3>¿A donde te lo enviamos?</h3>
                <p>Puedes agregar códigos de promoción.</p>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="chose_area">
                        <ul class="user_option">
                            <li>
                                <input type="checkbox">
                                <label>Usar Código de Cupón</label>
                            </li>
                            <li>
                                <input type="checkbox">
                                <label>Usar Voucher de Regalo</label>
                            </li>
                            <li>
                                <input type="checkbox">
                                <label>Calcular Envío</label>
                            </li>
                        </ul>
                        <ul class="user_info">
                            <li class="single_field">
                                <label>Pais:</label>
                                <select>
                                    <option>México</option>
                                    <option>United States</option>
                                    <option>Canada</option>
                                    <option>Dubai</option>
                                </select>

                            </li>
                            <li class="single_field">
                                <label>Región / Estado:</label>
                                <select>
                                    <option>Select</option>
                                    <option>Sonora</option>
                                    <option>Sinaloa</option>
                                    <option>Baja California</option>
                                    <option>Baja California Sur</option>
                                    <option>Chihuahua</option>
                                    <option>Ciudad de México</option>
                                    <option>Aguascalientes</option>
                                    <option>Monterrey</option>
                                    <option>Guadalajara</option>
                                </select>

                            </li>
                            <li class="single_field zip-field">
                                <label>Código Postal:</label>
                                <input type="text">
                            </li>
                        </ul>
                        <a class="btn btn-default update" href="">Calcular</a>
                        <a class="btn btn-default check_out" href="">Continuar</a>
                    </div>
                </div>
                <?php
                //calcular totales 
                ?>
                <div class="col-sm-6">
                    <div class="total_area">
                        <ul>
                            <li>Sub Total de Carrito <span>$<?php echo printf("%.1f", $subtotal / 1.16); ?></span>
                            </li>
                            <li>IVA <span>$<?php echo printf("%.1f", (($subtotal / 1.16) * 0.16)); ?></span></li>
                            <li>Costo de Envio<span>Free</span></li>
                            <li>Total <span>$<?php echo printf("%.1f", $subtotal); ?></span></li>
                        </ul>
                        <a class="btn btn-default update" href="">Actualizar</a>
                        <a class="btn btn-default check_out" href="">Pagar Carrito</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/#do_action-->

    <!-- footer -->
    <?php require 'includes/footer.php' ?>



    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>

</html>
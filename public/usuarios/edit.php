<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>PHP OOP CRUD TUTORIAL</title>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-md-12 mt-5">
          <h1 class="text-center">EDITAR PRODCUTOS</h1>
          <hr style="height: 1px;color: black;background-color: black;">
        </div>
      </div>
      <div class="row">
        <div class="col-md-5 mx-auto">
          <?php

              include 'model.php';
              $model = new Model();
              $id = $_REQUEST['id'];
              $row = $model->edit($id);

              if (isset($_POST['update'])) {
                if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['mobile']) && isset($_POST['address'])) {
                  if (!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['mobile']) && !empty($_POST['address']) ) {
                    
                    $data['id'] = $id;
                    $nombre = $_POST['nombre'];
                    $descripcion = $_POST['descripcion'];
                    $precio = $_POST['precio'];
                    $codBarra = $_POST['codBarra'];
                    $imagen = $_POST['imagen'];
                    $marca_id = $_POST['marca_id'];
                    $categoria_id = $_POST['categoria_id'];

                    $update = $model->update($data);

                    if($update){
                      echo "<script>alert('productos update successfully');</script>";
                      echo "<script>window.location.href = 'records.php';</script>";
                    }else{
                      echo "<script>alert('productos update failed');</script>";
                      echo "<script>window.location.href = 'records.php';</script>";
                    }

                  }else{
                    echo "<script>alert('empty');</script>";
                    header("Location: edit.php?id=$id");
                  }
                }
              }

              ?>
              <form action="" method="post">
                <div class="form-group">
                  <label for="">Nombre</label>
                  <input type="text" name="nombre" class="form-control">
                </div>
                <div class="form-group">
                  <label for="">Descripcion</label>
                  <textarea name="descripcion" id="" cols="" rows="3" class="form-control"></textarea>
                </div>
                <div class="form-group">
                  <label for="">Precio $</label>
                  <input type="text" name="precio" class="form-control">
                </div>
                <div class="form-group">
                  <label for="">Codigo Barra </label>
                  <input type="text" name="codBarra" class="form-control">
                </div>
                <div class="form-group">
                  <label for="">Imagen </label>
                  <input type="text" name="imagen" class="form-control">
                </div>
                <div class="form-group">
                  <label for="">Marca id </label>
                  <input type="text" name="marca_id" class="form-control">
                </div>
                <div class="form-group">
                  <label for="">Categoria id</label>
                  <input type="text" name="categoria_id" class="form-control">
                </div>
            
                
                <div class="form-group">
                  <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
          </form>
        </div>
      </div>
    </div>


  </body>
</html>
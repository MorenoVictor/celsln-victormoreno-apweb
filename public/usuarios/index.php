<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CELSLN</title>
    <link rel="stylesheet" href="/CelSln/public/css/fontello.css">
    <link rel="stylesheet" href="/CelSln/public/css/estilos1.css">
</head>
<body>
    <header>
        <div class="contenedor">
            <h1 class="icon-diamond">CELSLN</h1>
            <input type="checkbox" id="menu-bar">
            <label class="icon-menu" for="menu-bar"></label>
            <nav class="menu">
                <a href="login.php">Login</a>
                <a href="index.php">Inicio</a>
                <a href="productos.php">Productos</a>
                <a href="usuarios.php">Usuarios</a>
                <a href="proveedores.php">Proveedores</a>
            </nav>
        </div>
    </header>
    <main>
        <section id="banner">
            <img src="\CelSln\public\imagenes\omar-prestwich-HSd2y7VjX-E-unsplash.jpg">
            <div class="contenedor">
                <h2>USUARIOS HOME</h2>
                <p>Indicaciones</p>
                <a href="#">Leer más</a>
            </div>
        </section>
        <section id="bienvenidos">
            <h2>QUIENES SOMOS</h2>
            <p>Para traer siempre las últimas tendencias a la tienda, Bershka aprovecha la flexibilidad de su modelo de negocio para adaptarse
                 a los cambios que pueden producirse durante las campañas y, de este modo, reaccionar a ellos con nuevos productos en las tiendas 
                 en el período de tiempo más breve posible. Los modelos de cada campaña son desarrollados íntegramente por sus equipos creativos, 
                 que toman como principales fuentes de inspiración tanto las tendencias de moda imperantes en el mercado como a los propios clientes, a través de la información que se recibe de las tiendas. El equipo de diseñadores de Bershka está formado por más de 60 profesionales que evalúan constantemente las necesidades, deseos y demandas de los consumidores, ofreciendo cada año más de 4000 productos diferentes en sus tiendas.</p>
            
        </section>
        <section id="blog">
            <h3>Lo mas vendido</h3>
            <div class="contenedor">
                <article>
                    <img src="\CelSln\public\imagenes\isaac-smith-UcjsZ6ZqZIo-unsplash.jpg" ><br>
                <br><h4>TECNOLOGIA</h4>
                </article>
                <article>
                    <img src="\CelSln\public\imagenes\grysell-alvarez-FQXbLmlmvWY-unsplash.jpg" ><br>
                    <br><h4> MAS COMPRADOS</h4>
                </article>
                <article>
                    <img src="\CelSln\public\imagenes\andre-hunter-EHLbBpuZWVQ-unsplash.jpg" ><br>
                    <br><h4>EXCELENTE CALIDAD </h4>
                </article>
            </div>
        </section>
        <section id="info">
            <h3>Lo que mas se busca.</h3>
            <div class="contenedor">
                <div class="info-pet">
                    <img src="\CelSln\public\imagenes\hello-i-m-nik-5_ImgwicW_0-unsplash.jpg" alt="">
                    <h4>AUDIO</h4>
                </div>
                <div class="info-pet">
                    <img src="\CelSln\public\imagenes\aranprime-YuNXkQl_8gs-unsplash.jpg" alt="">
                    <h4>CASES</h4>
                </div>
                <div class="info-pet">
                    <img src="\CelSln\public\imagenes\caleb-woods-VVuRLhyTmXM-unsplash.jpg" alt="">
                    <h4>CELULARES</h4>
                </div>
                <div class="info-pet">
                    <img src="\CelSln\public\imagenes\jordan-cormack-1fiLXawz_ro-unsplash.jpg" alt="">
                    <h4>SEGURIDAD</h4>
                </div>
            </div>
        </section>
    </main>

    <footer>
        <div class="contenedor">
            <p class="copy">CELSLN &copy;2020</p>
            <div class="sociales">
                <a class="icon-youtube" href="#">CELSLN</a>
                <a class="icon-twitter" href="#">CELSLN</a>
                <a class="icon-instagram" href="#">CELSLN</a>
                <a class="icon-whatsapp" href="#">CELSLN</a>
            </div>
        </div>
    </footer>

</body>
</html>

<?php
session_start(); //se va a checar $_SESSION, se necesita 'session_start()'
//verificar que el cliente se ha logeado
if (isset($_SESSION['clienteLogeado']) && $_SESSION['clienteLogeado'] != null) {
    //si hay logeado, se muestra
    // header('location:./public/cart.php');
    echo 'YA ESTAS LOGEADO';
} else {

    //required CLIENTE
   // require_once "../app/middleware/clientes/Cliente.php";
    //si se ingreso datos en el FORM
    $mail = "";
    $pass = "";
    if (isset($_POST['btnLogin'])) {
        //giardamos los datos
        if (isset($_POST['correoLogin']))
            $mail = $_POST['correoLogin'];
        if (isset($_POST['passwordLogin']))
            $pass = $_POST['passwordLogin'];
        //creamos la instancia
        $cliente = new Cliente('', '', '', '', '', '', '', '', '');
        $logeado = 'nada';
        //ejecutamos el login
        if (($logeado = $cliente->login($mail, $pass)) && $logeado != null) {
            // var_dump($logeado);
            // die();
            //crear las vars de sesión
            $_SESSION['clienteLogeado'] = $logeado[0];
            var_dump($_SESSION['clienteLogeado']);
            //die();
            //abrimos HOME
            header("location:cuenta.php");
        } else {
            echo "Error al ingresar, verifique sus datos e inténtelo de nuevo";
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CELSLN</title>
    <link rel="stylesheet" href="/CelSln/public/css/fontello.css">
    <link rel="stylesheet" href="/CelSln/public/css/estilos1.css">
</head>
<!--/head-->


<body>
<header>
        <div class="contenedor">
            <h1 class="icon-diamond">CELSLN</h1>
            <input type="checkbox" id="menu-bar">
            <label class="icon-menu" for="menu-bar"></label>
            <nav class="menu">
                <a href="login.php">Login</a>
                <a href="index.php">Inicio</a>
                <a href="productos.php">Productos</a>
                <a href="usuarios.php">Usuarios</a>
                <a href="proveedores.php">Proveedores</a>
            </nav>
        </div>
    </header>
<section id="banner">
            <img src="\CelSln\public\imagenes\jakob-owens-Nrf3yY0kxCM-unsplash.jpg">
            <div class="contenedor">
                <h2>LOGIN USUARIOS</h2>
                <a href="#">Leer más</a>
            </div>
        </section>
    <section id="form">
        <!--form-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-sm-offset-1">
                    <div class="login-form">
                        <!--login form-->
                        <h2>Ingresa a tu Cuenta</h2>
                        <form action="" method="POST">
                            <input type="email" placeholder="Correo" name="correoLogin" />
                            <input type="password" placeholder="Contraseña" name="passwordLogin" />
                            <span>
                                <input type="checkbox" class="checkbox">
                                Mantenme contectado
                            </span>
                            <button type="submit" name="btnLogin" class="btn btn-default">Login</button>
                        </form>
                    </div>
                    <!--/login form-->
                </div>
                <div class="col-sm-1">
                    <h2 class="or">Ó</h2>
                </div>
                <div class="col-sm-4">
                    <div class="signup-form">
                        <!--sign up form-->
                        <h2>¡Registro de nuevo cliente!</h2>
                        <form action="" method="POST">
                            <input type="text" placeholder="Nombre" name="nombreRegistro" />
                            <input type="email" placeholder="Correo" name="correoRegistro" />
                            <input type="text" placeholder="Contraseña" name="passwordRegistro" />
                            <button type="submit" class="btn btn-default">Registrarse</button>
                        </form>
                    </div>
                    <!--/sign up form-->
                </div>
            </div>
        </div>
    </section>
    <!--/form-->


    <!-- footer -->
    <footer>
        <div class="contenedor">
            <p class="copy">CELSLN &copy;2020</p>
            <div class="sociales">
                <a class="icon-youtube" href="#">CELSLN</a>
                <a class="icon-twitter" href="#">CELSLN</a>
                <a class="icon-instagram" href="#">CELSLN</a>
                <a class="icon-whatsapp" href="#">CELSLN</a>
            </div>
        </div>
    </footer>

    <script src="js/jquery.js"></script>
    <script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>

</html>
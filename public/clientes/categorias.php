<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CELSLN</title>
    <link rel="stylesheet" href="/CelSln/public/css/fontello.css">
    <link rel="stylesheet" href="/CelSln/public/css/estilos1.css">
</head>
<body>
    <header>
        <div class="contenedor">
            <h1 class="icon-diamond">CELSLN</h1>
            <input type="checkbox" id="menu-bar">
            <label class="icon-menu" for="menu-bar"></label>
            <nav class="menu">
            <a href="index.php">Inicio</a>
                <a href="categorias.php">Categorias</a>
                <a href="cuenta.php">Cuenta</a>
                <a href="productos.php">Vista productos</a>
                <a href="carrito.php">Carrito de compras</a>
            </nav>
        </div>
    </header>
    <main>
        <section id="banner">
            <img src="\CelSln\public\imagenes\szabo-viktor-i9N3yaA2WWA-unsplash.jpg">
            <div class="contenedor">
                <h2>CATEGORIAS</h2>
                <p>Indicaciones</p>
                <a href="#">Leer más</a>
            </div>
        </section>
        <section id="blog">
            <h3>CATEGORIAS</h3>
            <div class="contenedor">
                <article>
                    <img src="\CelSln\public\imagenes\arnel-hasanovic-4oWSXdeAS2g-unsplash.jpg" ><br>
                <br><h4><a href=productos.php>CELULARES</h4>
                </article>
                <article>
                    <img src="\CelSln\public\imagenes\grysell-alvarez-FQXbLmlmvWY-unsplash.jpg" ><br>
                    <br><h4>SEGURIDAD</h4>
                </article>
                <article>
                    <img src="\CelSln\public\imagenes\andre-hunter-EHLbBpuZWVQ-unsplash.jpg" ><br>
                    <br><h4>AUDIO </h4>
                </article>
                <article>
                    <img src="\CelSln\public\imagenes\andre-hunter-EHLbBpuZWVQ-unsplash.jpg" ><br>
                    <br><h4>CABLES </h4>
                </article>
            </div>
        </section>
        
    </main>

    <footer>
        <div class="contenedor">
            <p class="copy">CELSLN &copy;2020</p>
            <div class="sociales">
                <a class="icon-youtube" href="#">CELSLN</a>
                <a class="icon-twitter" href="#">CELSLN</a>
                <a class="icon-instagram" href="#">CELSLN</a>
                <a class="icon-whatsapp" href="#">CELSLN</a>
            </div>
        </div>
    </footer>

</body>
</html>
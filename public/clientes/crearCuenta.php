 

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CELSLN</title>
    <link rel="stylesheet" href="/CelSln/public/css/fontello.css">
    <link rel="stylesheet" href="/CelSln/public/css/estilos1.css">
</head>
<body>
    <header>
        <div class="contenedor">
            <h1 class="icon-diamond">CELSLN</h1>
            <input type="checkbox" id="menu-bar">
            <label class="icon-menu" for="menu-bar"></label>
            <nav class="menu">
            <a href="index.php">Inicio</a>
                <a href="categorias.php">Categorias</a>
                <a href="cuenta.php">Cuenta</a>
                <a href="productos.php">Vista productos</a>
                <a href="carrito.php">Carrito de compras</a>
                <a href="crearCuenta.php">Crear Cuenta</a>
            </nav>
        </div>
    </header>
    <main>
        <section id="banner">
            <img src="\CelSln\public\imagenes\pexels-pixabay-267569.jpg">
            <div class="contenedor">
                <h2>CREAR CUENTA</h2>
                <p>Indicaciones</p>
                <a href="#">Leer más</a>
            </div>
        </section>
        <section id="blog">


    <?php

            if(isset($_POST['submit'])){

                include '../../app/middleware/clientes/Cliente.php';

                if(isset($_POST['password'])){
                    $password = $_POST['password'];
                }

                if(isset($_POST['nombre'])){
                    $nombre = $_POST['nombre'];
                }

                if(isset($_POST['apellidoP'])){
                    $apellidoP = $_POST['apellidoP'];

                }
                if(isset($_POST['apellidoM'])){
                    $apellidoM = $_POST['apellidoM'];
                }
                if(isset($_POST['fechaN'])){
                    $fechaN = $_POST['fechaN'];
                }
                if(isset($_POST['numCel'])){
                    $numCel = $_POST['numCel'];
                }
                if(isset($_POST['correo'])){
                    $correo = $_POST['correo'];
                    var_dump($correo);// die();
                }
                if(isset($_POST['calle'])){
                    $calle = $_POST['calle'];
                }
                if(isset($_POST['num'])){
                    $num = $_POST['num'];
                }
                if(isset($_POST['colonia'])){
                    $colonia = $_POST['colonia'];
                }
                if(isset($_POST['codigoPostal'])){
                    $cp = $_POST['codigoPostal'];
                }
                if(isset($_POST['localidad'])){
                    $localidad = $_POST['localidad'];
                }
                if(isset($_POST['municipio'])){
                    $municipio = $_POST['municipio'];
                }


            $cliente = new Cliente($password,$nombre,$apellidoP,$apellidoM,$fechaN,$numCel,$correo,$calle,$num,$colonia,$cp,$localidad,$municipio);

            $cliente->insertar();

            }


            

            //Usuario::InsertarCliente();

            //echo  $usuario;

?>

    
            
        </section>

     
                <div class="col-sm-4">
                    <div class="signup-form">
                        <!--sign up form-->
                        <h2>¡Registro de nuevo cliente!</h2>
                        <form action="" method="post">
            
            <div class="form-group">
              <label for="">Password :</label>
              <input type="text" name="password" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Nombre :</label>
              <input type="text" name="nombre" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Apellido P: </label>
              <input type="text" name="apellidoP" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Apellido M: </label>
              <input type="text" name="apellidoM" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Fecha Nac: </label>
              <input type="text" name="fechaN" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Num celular:</label>
              <input type="text" name="numCel" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Correo:</label>
              <input type="text" name="correo" class="form-control">
            </div>
           
            <div class="form-group">
              <label for="">Calle:</label>
              <input type="text" name="calle" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Num:</label>
              <input type="text" name="num" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Colonia:</label>
              <input type="text" name="colonia" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Codigo Postal</label>
              <input type="text" name="codigoPostal" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Localidad:</label>
              <input type="text" name="localidad" class="form-control">
            </div>
            <div class="form-group">
              <label for="">Municipio:</label>
              <input type="text" name="municipio" class="form-control">
            </div>
            
            
            <div class="form-group">
              <button type="submit" name="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
                    </div>
                    <!--/sign up form-->
                </div>
            </div>
        </div>
    </section>
        
    </main>
    
    <footer>
        <div class="contenedor">
            <p class="copy">CELSLN &copy;2020</p>
            <div class="sociales">
                <a class="icon-youtube" href="#">CELSLN</a>
                <a class="icon-twitter" href="#">CELSLN</a>
                <a class="icon-instagram" href="#">CELSLN</a>
                <a class="icon-whatsapp" href="#">CELSLN</a>
            </div>
        </div>
    </footer>

</body>
</html>
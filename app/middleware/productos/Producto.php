<?php
//incluimos la LibBD
require_once dirname(dirname(__FILE__)) . "/bd/BDLib.php";
require_once dirname(dirname(__FILE__)) . "/bd/CriterioDeBusqueda.php";
require_once "Marca.php";
require_once "Categoria.php";

class Producto
{
    //miembros
    //props
    private $id;
    private $nombre;
    private $descripcion;
    private $precio;
    private $codBarra;
    private $imagen;
    private $marcaId;
    private $categoriaId;
    
    //ob de libBD
    private $bd;
    //constructor conex
    public function __construct($id, $nom, $descrip, $precio, $cod, $img, $marca = null, $categoria = null)
    {
        $this->id = $id;
        $this->nombre = $nom;
        $this->descripcion = $descrip;
        $this->precio = $precio;
        $this->codBarra = $cod;
        $this->imagen = $img;
        //var_dump($this); //die();
        $this->marcaId = $marca != null ? $marca : new Marca();
        $this->categoriaId = $categoria;
        //crea conexion
        $this->bd = new BDLib();
    }
    //destructor!?!??!  desconex
    public function __destruct()
    {
    }
    //métodos

    //CRUDS------------------------
    public function insertar()
    {
        //los datos de los campos a guardar
        $campos = "nombre,descripcion, precio,codBarra,imagen,marca_id,categoria_id";

        $valores = "'" . $this->getNombre() . "','" . $this->getDescripcion() . "','" . $this->getPrecio() . "'," . $this->getCodBarras() . ",'" . $this->getImagenes() . "'," . $this->getMarca() . "," . $this->getCategoria() . "";
        //"nombre, 'descripcion', 'precio','marca_id', 'categoria_id', 'codigo_barras','imagen_producto', 'unidad_medida', 'es_perecedero'";

       //var_dump($campos,$valores); die();

        return $this->bd->insertar('productos', $campos, $valores);
    }
    public function update($datosActuales, $id)
    {

        return $this->bd->actualizar('productos', $datosActuales, $id);
    }
    public function eliminar($id)
    {
        return $this->bd->eliminar("productos", $id);
    }

    public function consultaPorNombre($nombre, $descrip)
    {
    }
    public function consultaPorPrecio($nombre, $descrip)
    {
    }
    //métodos
    public function consultarTodos()
    {
        return $this->bd->consultarTodos('productos');
    }

    public function consultaPorID($id)
    {
        return $this->bd->consultarPorID('productos', '*', $id);
    }

    public function getNombreDeMarca($idMarca)
    {
        return $this->bd->consultarPorID('marcas', 'nombre', $idMarca)[0][0];
    }

    public function getNombreDeCategoria($idCatego)
    {
        return $this->bd->consultarPorID('categorias', 'nombre', $idCatego)[0][0];
    }


    public function consultarPorCriterios($criterios)
    {
        return $this->bd->consultarConArrayCriterios('productos', '*', $criterios);
    }

    public function actualizarPrecios($listaPreciosNuevos)
    {
    }
    public function actualizarPrecio($precioNuevo, $id)
    {
    }

    /**
     * Get the value of descripcion
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set the value of descripcion
     *
     * @return  self
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of nombre
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of precio
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set the value of precio
     *
     * @return  self
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get the value of codBarras
     */
    public function getCodBarras()
    {
        return $this->codBarra;
    }

    /**
     * Set the value of codBarras
     *
     * @return  self
     */
    public function setCodBarras($codBarra)
    {
        $this->codBarra = $codBarra;

        return $this;
    }

    /**
     * Get the value of imagenes
     */
    public function getImagenes()
    {
        return $this->imagen;
    }

    /**
     * Set the value of imagenes
     *
     * @return  self
     */
    public function setImagenes($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get the value of marca
     */
    public function getMarca()
    {
        return $this->marcaId;
    }

    /**
     * Set the value of marca
     *
     * @return  self
     */
    public function setMarca($marcaId)
    {
        $this->marcaId = $marcaId;

        return $this;
    }

    /**
     * Get the value of categoria
     */
    public function getCategoria()
    {
        return $this->categoriaId;
    }

    /**
     * Set the value of categoria
     *
     * @return  self
     */
    public function setCategoria($categoriaId)
    {
        $this->categoriaId = $categoriaId;

        return $this;
    }

   

   
    
}
<?php

//incluimos la LibBD
require_once dirname(dirname(__FILE__)) . "/bd/BDLib.php";
require_once dirname(dirname(__FILE__)) . "/bd/CriterioDeBusqueda.php";

class Usuario
{
     //props de Usuario de pdv UTH 
     private $id;
     private $tipoUsuario;
     private $password;
     private $nombre;
     private $apellidoPaterno;
     private $apellidoMaterno;
     private $fechaDeNacimiento;
     private $celular;
     private $correo;
     private $calle;
     private $num;
     private $colonia;
     private $cp;
     private $localidad;
     private $municipio;

      //BD
    private $bd;

	public function getId() {
		return $this->id;
	}

	public function setId( $id) {
		$this->id = $id;
	}

    public function getTipoUsuario() {
		return $this->tipoUsuario;
	}

	public function setTipoUsuario( $tipoUsuario) {
		$this->tipoUsuario = $tipoUsuario;
    }
    
	public function getNombre() {
		return $this->nombre;
	}

	public function setNombre( $nombre) {
		$this->nombre = $nombre;
	}

	public function getApellidoPaterno() {
		return $this->apellidoPaterno;
	}

	public function setApellidoPaterno( $apellidoPaterno) {
		$this->apellidoPaterno = $apellidoPaterno;
	}

	public function getApellidoMaterno() {
		return $this->apellidoMaterno;
	}

	public function setApellidoMaterno( $apellidoMaterno) {
		$this->apellidoMaterno = $apellidoMaterno;
	}

	public function getFechaDeNacimiento() {
        $date = new DateTime($this->fechaDeNacimiento);
        //var_dump();
        return $date->format('Y-m-d');
        $this->fechaDeNacimiento;
         
	}

	public function setFechaDeNacimiento( $fechaDeNacimiento) {
		$this->fechaDeNacimiento = $fechaDeNacimiento;
	}

	public function getCelular() {
		return $this->celular;
	}

	public function setCelular( $celular) {
		$this->celular = $celular;
	}

	public function getTelefono() {
		return $this->telefono;
	}

	public function setTelefono( $telefono) {
		$this->telefono = $telefono;
	}

	public function getCorreo() {
		return $this->correo;
	}

	public function setCorreo( $correo) {
		$this->correo = $correo;
    }
    
    public function getCalle() {
		return $this->calle;
	}

	public function setCalle( $calle) {
		$this->calle = $calle;
    }

    public function getNum() {
		return $this->num;
	}

	public function setNum( $num) {
		$this->num = $num;
    }
    public function getColonia() {
		return $this->colonia;
	}

	public function setColonia( $colonia) {
		$this->colonia = $colonia;
    }

    public function getCp() {
		return $this->cp;
	}

	public function setCp( $cp) {
		$this->cp = $cp;
    }

    public function getLocalidad() {
		return $this->localidad;
	}

	public function setLocalidad( $localidad) {
		$this->localidad = $localidad;
    }

    public function getMunicipio() {
		return $this->municipio;
	}

	public function setMunicipio( $municipio) {
		$this->municipio = $municipio;
    }

	public function getPassword() {
		return $this->password;
	}

	public function setPassword( $password) {
		$this->password = $password;
    }
    
    

    //faltan los datos de domicilio, curo, etc

   
    //construct
    public function __construct( $tipoU,$pass, $nom, $apP, $apM, $fecha, $cel, $mail, $call, $num, $col, $cp, $loc, $mun)
    {
        //inicializa props
    
        $this->tipoUsuario = $tipoU;
        $this->password = $pass;
        $this->nombre = $nom;
        $this->apellidoPaterno = $apP;
        $this->apellidoMaterno = $apM;
        $this->fechaDeNacimiento = $fecha;
        $this->celular = $cel;
        $this->correo = $mail;
        $this->calle = $call;
        $this->num = $num;
        $this->colonia = $col;
        $this->cp = $cp;
        $this->localidad = $loc;
        $this->municipio = $mun;
       
       
        //crea conexion
        $this->bd = new BDLib();

        //var_dump($this); die();
    }
     //Hoy 07/08/2020
    public function __toString()
    {
        return "Info de usuario es :  ".$this->nombre;
    }

    //CRUDSSS

    //login
    public function login($correo, $pass)
    {
        //hacer consulta
        $criterios = [
            "0" => new CriterioDeBusqueda('correo', CriterioDeBusqueda::OP_IGUAL, $correo, true, CriterioDeBusqueda::OP_LOGICO_AND),
            "1" => new CriterioDeBusqueda('password', CriterioDeBusqueda::OP_IGUAL, $pass, true, CriterioDeBusqueda::OP_LOGICO_NONE),
        ];
        //SELECT * FROM clientes WHERE correo = '??' AND password = '??' ..... 
        //consulta
        return $this->bd->consultarConArrayCriterios('usuarios', '*', $criterios);
    }

    //CRUDS------------------------
    public function insertar()
    {
        //los datos de los campos a guardar
        $campos = "tipo_usuario,password,nombre,apellidoP,apellidoM,fechaN,numCel,correo,calle,num,colonia,cp,localidad,municipio";

        $valores = "'" . $this->getTipoUsuario() . "','" . $this->getPassword() . "','" . $this->getNombre() . "','" . $this->getApellidoPaterno() . "','" . $this->getApellidoMaterno() . "','" . $this->getFechaDeNacimiento() . "','" . $this->getCelular() . "','" . $this->getCorreo() . "','" . $this->getCalle() ."','" . $this->getNum() ."','" . $this->getColonia() . "','" . $this->getCP() ."','" . $this->getLocalidad() ."','" . $this->getMunicipio() ."'";
        

        return $this->bd->insertar('usuarios', $campos, $valores);
    }

    //get set

}